//
//  RoundedButton.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 31/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {

    @IBInspectable var cornerRadius: CGFloat = 0{
        didSet{
            self.layer.cornerRadius = self.frame.height / 2
        }
    }

}
