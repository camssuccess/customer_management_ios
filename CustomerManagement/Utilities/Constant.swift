//
//  Constant.swift
//  Music App
//
//  Created by KUMAR GAURAV on 04/04/20.
//  Copyright © 2020 Music. All rights reserved.
//

import UIKit
import Foundation
import BRYXBanner
import AudioToolbox


let DEVICE_TOKEN = "Device_token"

let nc = NotificationCenter.default
let defaults = UserDefaults.standard
let MY_COLLECTIONS = "my_collections_keys"
let COLLECTIONS_DATE_TIME = "collections_date_time"
let kBaseURL = "https://bharatscan.sortstring.com/api/"
let USER_ID_LOGIN = "USERID"
let MOBILE = "mobile"
let IS_LOGGED_IN = "isLoggedIn"

let LOGIN_FACEBOOK = "facebook"
let FACEBOOK_SCHEME = "fb325219008529254"
let GOOGLE_SCHEME = "260203951813-8hul0nc7c765of7e40nlirnigoeb6jms.apps.googleusercontent.com"
let TWITTER_SCHEME = ""
let LOGIN_GOOGLE = "google"

let USER_ID = "id"
let IS_LOGIN = "isLogin"
///-------------------------------Color Code ------------------------------------
let System_Gray_2 = "AEAEB2"
///---------------------------------------------------
let COLOR_SYSTEM_GRAY_2 : UIColor = hexStringToUIColor(hex: System_Gray_2)
let documentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

var str_UserID : String?
func getUserID()->String{
    if let str_UserID = (UserDefaults.standard.value(forKey: USER_ID_LOGIN) as? String){
    return str_UserID
    }
    else{
        return ""
    }
}
let image_dot_pink = UIImage(named: "dot_pink")


// Screen width.
public var screenWidth: CGFloat {
    return UIScreen.main.bounds.width
}

// Screen height.
public var screenHeight: CGFloat {
    return UIScreen.main.bounds.height
}

let themeColor = UIColor(red: 87.0/255.0, green: 182.0/255.0, blue: 195.0/255.0, alpha: 1.0)
let themeColor_blue = UIColor(red: 0/255.0, green: 174.0/255.0, blue: 239.0/255.0, alpha: 1.0)

func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
    let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
    label.numberOfLines = 0
    label.lineBreakMode = NSLineBreakMode.byWordWrapping
    label.font = font
    label.text = text
    label.sizeToFit()
    return label.frame.height
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt64 = 0
    Scanner(string: cString).scanHexInt64(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}



extension UIView{
    func addGradient(color1 : UIColor , color2 : UIColor){
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [color1.cgColor, color2.cgColor]
        gradient.startPoint = CGPoint(x: 1, y: 1)
        gradient.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func addGradient_withBlue(){
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = [themeColor.cgColor, UIColor(red: 238/255.0, green: 63/255.0, blue: 119/255.0, alpha: 1.0).cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func fadeIn(_ duration: TimeInterval = 1.0, delay: TimeInterval = 4.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 1.0, delay: TimeInterval = 4.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    func MyToast(){
        let label = UILabel(frame: CGRect(x:0, y:self.frame.size.height / 2, width:220, height:40))
        label.backgroundColor = UIColor.red
        label.center = self.center
        label.textAlignment = NSTextAlignment.center
        label.clipsToBounds  = true
        label.layer.cornerRadius = 10
        label.textColor = UIColor.white
        self.addSubview(label)
        label.fadeIn(completion: {
            (finished: Bool) -> Void in
            label.text = "Network Unavailble !"
            label.fadeOut()
        })
    }
    
    func addShadow(color:UIColor){
        layer.shadowRadius = 2.0
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.init(width: 0, height:0)//CGSize.zero
    }
    func addSideShadow(color:UIColor){
        layer.shadowRadius = 2.0
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.init(width: 1.5, height: 1.5)//CGSize.zero
    }
    func addShadowWithExtraWidth(color:UIColor){
        layer.shadowRadius = 4.0
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.init(width: 3, height:3)//CGSize.zero
    }
    
}



func showAlert_banner(msg:String,bgColor:UIColor,type:String){
    let strMessage = msg
    let banner: Banner
    if type == "success" {
        banner = Banner(title: "Succes", subtitle: strMessage, image: UIImage(named: "approved"), backgroundColor: bgColor)
    }
    else{
        banner = Banner(title: "Alert", subtitle: strMessage, image: UIImage(named: "rejected"), backgroundColor: bgColor)
    }
    banner.dismissesOnTap = true
    banner.show(duration: 3.0)
}


// MARK: - Method to convert JSON String into Dictionary
func convertToDictionary(text: String) -> [String: Any]? {
    if let data = text.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
//            print(error.localizedDescription)
        }
    }
    return nil
}


extension String{
    var isNumber: Bool {
        return !isEmpty && rangeOfCharacter(from: CharacterSet.decimalDigits.inverted) == nil
    }
}




func get_user_id()-> String{
    return defaults.value(forKey: USER_ID) as! String
}
extension UIView {
    func roundCorners_AtBottom( radius: CGFloat) {
        self.clipsToBounds = true
        self.layer.cornerRadius = radius
        if #available(iOS 11.0, *) {
            self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

//            self.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        } else {
            let corners = CACornerMask()
            var cornerMask = UIRectCorner()
//                 if(corners.contains(.layerMinXMinYCorner)){
//                     cornerMask.insert(.topLeft)
//                 }
//                 if(corners.contains(.layerMaxXMinYCorner)){
//                     cornerMask.insert(.topRight)
//                 }
                 if(corners.contains(.layerMinXMaxYCorner)){
                     cornerMask.insert(.bottomLeft)
                 }
                 if(corners.contains(.layerMaxXMaxYCorner)){
                     cornerMask.insert(.bottomRight)
                 }
//                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

                 let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
                 let mask = CAShapeLayer()
                 mask.path = path.cgPath
                 self.layer.mask = mask
            
        }
    }
    
        func roundCorners_AtTopRight( radius: CGFloat) {
            self.clipsToBounds = true
            self.layer.cornerRadius = radius
            if #available(iOS 11.0, *) {
                self.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
                print("if")
            } else {
                print("else")
                let corners = CACornerMask()
                var cornerMask = UIRectCorner()
    //                 if(corners.contains(.layerMinXMinYCorner)){
                         cornerMask.insert(.topLeft)
    //                 }
    //                 if(corners.contains(.layerMaxXMinYCorner)){
    //                     cornerMask.insert(.topRight)
    //                 }
                     if(corners.contains(.layerMinXMaxYCorner)){
//                         cornerMask.insert(.bottomLeft)
                     }
                     if(corners.contains(.layerMaxXMaxYCorner)){
//                         cornerMask.insert(.bottomRight)
                     }
    //                self.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]

                     let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: cornerMask, cornerRadii: CGSize(width: radius, height: radius))
                     let mask = CAShapeLayer()
                     mask.path = path.cgPath
                     self.layer.mask = mask
                
            }
        }
}

extension UIImage{
    func resize(toWidth width: CGFloat) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        return UIGraphicsImageRenderer(size: canvas, format: imageRendererFormat).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
}




 

//    func getImageFromDocumentDirectory(file_name : String)->UIImage
//    {
//        let fileManager = FileManager.default
//        var tempImage = UIImage()
//
//        let imagePath = (getDirectoryPath() as NSURL).appendingPathComponent(file_name + ".jpeg") // here assigned img name who assigned to img when saved in document directory. Here I Assigned Image Name "MyImage.png"
//
//        let urlString: String = imagePath!.absoluteString
//
//        if fileManager.fileExists(atPath: urlString)
//        {
//            let GetImageFromDirectory = UIImage(contentsOfFile: urlString) // get this image from Document Directory And Use This Image In Show In Imageview
//            tempImage = GetImageFromDirectory!
//        }
////        else
////        {
////            print("No Image Found")
////        }
//        return tempImage
//    }


// MARK: - Methods
extension UIAlertController {
    
    /// Present alert view controller in the current view controller.
    ///
    /// - Parameters:
    ///   - animated: set true to animate presentation of alert controller (default is true).
    ///   - vibrate: set true to vibrate the device while presenting the alert (default is false).
    ///   - completion: an optional completion handler to be called after presenting alert controller (default is nil).
    public func show(animated: Bool = true, vibrate: Bool = false, style: UIBlurEffect.Style? = nil, completion: (() -> Void)? = nil) {
        
        /// TODO: change UIBlurEffectStyle
        if let style = style {
            for subview in view.allSubViewsOf(type: UIVisualEffectView.self) {
                subview.effect = UIBlurEffect(style: style)
            }
        }
        
        DispatchQueue.main.async {
            UIApplication.shared.keyWindow?.rootViewController?.present(self, animated: animated, completion: completion)
            if vibrate {
                AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
            }
        }
    }
    
    /// Add an action to Alert
    ///
    /// - Parameters:
    ///   - title: action title
    ///   - style: action style (default is UIAlertActionStyle.default)
    ///   - isEnabled: isEnabled status for action (default is true)
    ///   - handler: optional action handler to be called when button is tapped (default is nil)
    func addAction(image: UIImage? = nil, title: String, color: UIColor? = nil, style: UIAlertAction.Style = .default, isEnabled: Bool = true, handler: ((UIAlertAction) -> Void)? = nil) {
        //let isPad: Bool = UIDevice.current.userInterfaceIdiom == .pad
        //let action = UIAlertAction(title: title, style: isPad && style == .cancel ? .default : style, handler: handler)
        let action = UIAlertAction(title: title, style: style, handler: handler)
        action.isEnabled = isEnabled
        
        // button image
        if let image = image {
            action.setValue(image, forKey: "image")
        }
        
        // button title color
        if let color = color {
            action.setValue(color, forKey: "titleTextColor")
        }
        
        addAction(action)
    }
    
    /// Set alert's title, font and color
    ///
    /// - Parameters:
    ///   - title: alert title
    ///   - font: alert title font
    ///   - color: alert title color
    func set(title: String?, font: UIFont, color: UIColor) {
        if title != nil {
            self.title = title
        }
        setTitle(font: font, color: color)
    }
    
    func setTitle(font: UIFont, color: UIColor) {
        guard let title = self.title else { return }
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        let attributedTitle = NSMutableAttributedString(string: title, attributes: attributes)
        setValue(attributedTitle, forKey: "attributedTitle")
    }
    
    /// Set alert's message, font and color
    ///
    /// - Parameters:
    ///   - message: alert message
    ///   - font: alert message font
    ///   - color: alert message color
    func set(message: String?, font: UIFont, color: UIColor) {
        if message != nil {
            self.message = message
        }
        setMessage(font: font, color: color)
    }
    
    func setMessage(font: UIFont, color: UIColor) {
        guard let message = self.message else { return }
        let attributes: [NSAttributedString.Key: Any] = [.font: font, .foregroundColor: color]
        let attributedMessage = NSMutableAttributedString(string: message, attributes: attributes)
        setValue(attributedMessage, forKey: "attributedMessage")
    }
    
    /// Set alert's content viewController
    ///
    /// - Parameters:
    ///   - vc: ViewController
    ///   - height: height of content viewController
    func set(vc: UIViewController?, width: CGFloat? = nil, height: CGFloat? = nil) {
        guard let vc = vc else { return }
        setValue(vc, forKey: "contentViewController")
        if let height = height {
            vc.preferredContentSize.height = height
            preferredContentSize.height = height
        }
    }
}


extension UIView {
    
    func searchVisualEffectsSubview() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let found = subview.searchVisualEffectsSubview() {
                    return found
                }
            }
        }
        return nil
    }
    
    /// This is the function to get subViews of a view of a particular type
    /// https://stackoverflow.com/a/45297466/5321670
    func subViews<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T{
                all.append(aView)
            }
        }
        return all
    }
    
    
    /// This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T
    /// https://stackoverflow.com/a/45297466/5321670
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T{
                all.append(aView)
            }
            guard view.subviews.count>0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
}


extension UIAlertController {
    
    /// Add a date picker
    ///
    /// - Parameters:
    ///   - mode: date picker mode
    ///   - date: selected date of date picker
    ///   - minimumDate: minimum date of date picker
    ///   - maximumDate: maximum date of date picker
    ///   - action: an action for datePicker value change
    
    func addDatePicker(mode: UIDatePicker.Mode, date: Date?, minimumDate: Date? = nil, maximumDate: Date? = nil, action: DatePickerViewController.Action?) {
        let datePicker = DatePickerViewController(mode: mode, date: date, minimumDate: minimumDate, maximumDate: maximumDate, action: action)
        set(vc: datePicker, height: 217)
    }
}


final class DatePickerViewController: UIViewController {
    
    public typealias Action = (Date) -> Void
    
    fileprivate var action: Action?
    
    fileprivate lazy var datePicker: UIDatePicker = { [unowned self] in
        $0.addTarget(self, action: #selector(DatePickerViewController.actionForDatePicker), for: .valueChanged)
        return $0
    }(UIDatePicker())
    
    required init(mode: UIDatePicker.Mode, date: Date? = nil, minimumDate: Date? = nil, maximumDate: Date? = nil, action: Action?) {
        super.init(nibName: nil, bundle: nil)
        datePicker.datePickerMode = mode
        datePicker.date = date ?? Date()
        datePicker.minimumDate = minimumDate
        datePicker.maximumDate = maximumDate
        self.action = action
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        print("has deinitiali   zed")
    }
    
    override func loadView() {
        view = datePicker
    }
    
    @objc func actionForDatePicker() {
        action?(datePicker.date)
    }
    
    public func setDate(_ date: Date) {
        datePicker.setDate(date, animated: true)
    }
}
