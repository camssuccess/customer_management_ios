//
//  CarSelection_Cell.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 03/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class CarSelection_Cell: UITableViewCell {

    @IBOutlet weak var bottom_tab_1: UIView!{
        didSet{
            bottom_tab_1.layer.cornerRadius = bottom_tab_1.frame.height / 2
            bottom_tab_1.addSideShadow(color: .gray)
        }
    }
    @IBOutlet weak var bottom_tab_2: UIView!{
        didSet{
            bottom_tab_2.layer.cornerRadius = bottom_tab_2.frame.height / 2
            bottom_tab_2.addSideShadow(color: .gray)

        }
    }
    @IBOutlet weak var bottom_tab_3: UIView!{
        didSet{
            bottom_tab_3.layer.cornerRadius = bottom_tab_3.frame.height / 2
            bottom_tab_3.addSideShadow(color: .gray)

        }
    }
    @IBOutlet weak var bottom_tab_4: UIView!{
        didSet{
            bottom_tab_4.layer.cornerRadius = bottom_tab_4.frame.height / 2
            bottom_tab_4.addSideShadow(color: .gray)

        }
    }
    
    @IBOutlet weak var slider_1: UISlider!{
        didSet{
            slider_1.setThumbImage(image_dot_pink, for: .normal)
            slider_1.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    
    @IBOutlet weak var slider_2: UISlider!{
        didSet{
        slider_2.setThumbImage(image_dot_pink, for: .normal)
        slider_2.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    
    
    @IBOutlet weak var slider_3: UISlider!{
        didSet{
            slider_3.setThumbImage(image_dot_pink, for: .normal)
            slider_3.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    
    @IBOutlet weak var slider_4: UISlider!{
        didSet{
        slider_4.setThumbImage(image_dot_pink, for: .normal)
        slider_4.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    
    
    @IBOutlet weak var view_bg: UIView!{
        didSet{
            view_bg.layer.cornerRadius = 12
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
