//
//  About_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//
import IHKeyboardAvoiding
import UIKit

class About_VC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var btn_submit: UIButton!

    @IBOutlet weak var txtField_name: UITextField!
    @IBOutlet weak var view_paging_unfocused: UIView!
    @IBOutlet weak var view_paging_focused: UIView!

    @IBOutlet weak var txtField_PinCode: UITextField!
    @IBOutlet weak var txtFiled_mobile: UITextField!
    @IBOutlet weak var txtField_dob: UITextField!
    @IBOutlet weak var view_code: UIView!
    @IBOutlet weak var view_mobile: UIView!
    @IBOutlet weak var view_password: UIView!
    @IBOutlet weak var view_username: UIView!
    @IBOutlet weak var view_bg: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUIConfiguration()
    }
    
    override func viewWillLayoutSubviews() {
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont(name: "CaviarDreams", size: 20)!]

    }

    func setUIConfiguration(){
        self.btn_submit.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        view_mobile.layer.cornerRadius = (self.view_mobile.frame.height / 2) - 5
        view_username.layer.cornerRadius = self.view_username.frame.height / 2
        view_code.layer.cornerRadius = 10

        view_password.layer.cornerRadius = self.view_password.frame.height / 2
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2
        view_username.addShadowWithExtraWidth(color: .gray)
        view_password.addShadowWithExtraWidth(color: .gray)
        
        view_mobile.addShadowWithExtraWidth(color: .gray)
        view_code.addShadowWithExtraWidth(color: .gray)
        self.txtField_name.delegate = self
        self.txtField_dob.delegate = self
        self.txtFiled_mobile.delegate = self
        self.txtField_PinCode.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)

        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_name!.resignFirstResponder()
        txtField_dob!.resignFirstResponder()
        txtFiled_mobile!.resignFirstResponder()
        txtField_PinCode!.resignFirstResponder()
    }
    
      @IBAction func submit_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarificationCode_VC") as! VarificationCode_VC
        vc.modalPresentationStyle  = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
      }
    

      @IBAction func back_Action(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }
 

}
