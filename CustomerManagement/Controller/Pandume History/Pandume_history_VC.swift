//
//  Pandume_history_VC.swift
//  CustomerManagement
//
//  Created by KUMAR GAURAV on 08/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class Pandume_history_VC: UIViewController {
    @IBOutlet weak var tblView_history: UITableView!

    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var view_card: UIView!
    var arr_history : [[String:Any]] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        arr_history = [
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"],
            ["payment_status":"RM 8","name":"Gaurav","wheel_price":"Audi 5","price":"100"]
        ]
        self.view_bg.roundCorners_AtTopRight(radius: 34)
        self.view_card.addSideShadow(color: .gray)
        self.view_card.layer.cornerRadius = 6
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


extension Pandume_history_VC : UITableViewDataSource,UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arr_history.count
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "History_Cell") as! History_Cell
        let newDic = arr_history[indexPath.item]
        
        cell.lbl_name.text = (newDic["name"] as! String)
        cell.lbl_price.text = (newDic["price"] as! String)
        cell.lbl_payment_status.text = (newDic["payment_status"] as! String)
        cell.lbl_wheel_price.text = (newDic["wheel_price"] as! String)
        cell.view_bg.layer.cornerRadius = 10
        cell.view_bg.addSideShadow(color: .gray)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 98
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RL_Confirmation") as! RL_Confirmation
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
