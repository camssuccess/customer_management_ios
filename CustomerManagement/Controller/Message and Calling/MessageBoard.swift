//
//  MessageBoard.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 04/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class MessageBoard: UIViewController {

    @IBOutlet weak var view_textFeild: UIView!{
        didSet{
            view_textFeild.layer.cornerRadius = view_textFeild.frame.height / 2
        }
    }
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var tblview_msgBoard: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear

    }
    
    
    @IBAction func camera_Action(_ sender: Any) {
    }
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func send_Action(_ sender: Any) {
    }
}
