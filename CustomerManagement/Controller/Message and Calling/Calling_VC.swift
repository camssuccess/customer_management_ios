//
//  Calling_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 04/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class Calling_VC: UIViewController {

    @IBOutlet weak var img_profile: UIImageView!{
        didSet{
            img_profile.layer.cornerRadius = img_profile.frame.height / 2
            img_profile.layer.borderColor = UIColor.white.cgColor
            img_profile.layer.borderWidth = 10
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

   @IBAction func back_Action(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }

}
