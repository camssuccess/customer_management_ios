//
//  VarificationCode_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class VarificationCode_VC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var view_otp: UIView!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_paging_focused: UIView!
    @IBOutlet weak var view_paging_unfocused: UIView!

    @IBOutlet weak var btn_resned: UIButton!
    @IBOutlet weak var txtField_otp: UITextField!
    @IBOutlet weak var lbl_timer: UILabel!
    
    @IBOutlet weak var view_bg: UIView!
    var OTPTimer: Timer?
    var count = 120

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUIConfiguration()
    }
    
    
    override func viewWillLayoutSubviews() {
        
        OTPTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(runTimedCode), userInfo: nil, repeats: true)
        self.btn_resned.isEnabled = false
//        self.btn_resned.titleColor(.red for: .normal)
        //        self.btn_resned.isSelected = false
        
    }
    

    func setUIConfiguration(){
        self.btn_submit.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        view_otp.layer.cornerRadius = (self.view_otp.frame.height / 2)
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2
        view_otp.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        self.txtField_otp.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)

        
    }
    
    
    @objc func runTimedCode(){
        if(count > -1){
            let minutes = String(count / 60)
            var seconds = String(count % 60)
            if seconds == "0"{
                seconds = "00"
            }
            self.lbl_timer.text = "0" + minutes + ":" + seconds
            count = count - 1
        }
        else{
            self.btn_resned.isEnabled = true
            self.btn_resned.isSelected = true

        }
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_otp!.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg
    }


    @IBAction func resend_Action(_ sender: Any) {
        print("resend clicked")
    }
    
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submit_Action(_ sender: Any) {
      let vc = self.storyboard?.instantiateViewController(withIdentifier: "VarificationCode_VC") as! VarificationCode_VC
      vc.modalPresentationStyle  = .fullScreen
      self.navigationController?.pushViewController(vc, animated: true)
    }
}
