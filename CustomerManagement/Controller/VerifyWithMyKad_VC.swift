//
//  VerifyWithMyKad_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 28/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class VerifyWithMyKad_VC: UIViewController {

    @IBOutlet weak var btn_veryfy: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillLayoutSubviews() {
        setUIConfiguration()
    }

    func setUIConfiguration(){
        self.btn_veryfy.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_veryfy.layer.cornerRadius = self.btn_veryfy.frame.height / 2
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
