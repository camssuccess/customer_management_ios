//
//  YourComfort_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 28/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
class YourComfort_VC: UIViewController,UITextFieldDelegate {
//--------------------Transmission type -------------------
    @IBOutlet weak var btn_auto: UIButton!
    @IBOutlet weak var btn_manual: UIButton!
    //--------------------Transmission type -------------------
    @IBOutlet weak var btn_sedan: UIButton!
    @IBOutlet weak var btn_SUV: UIButton!
    @IBOutlet weak var btn_MPV: UIButton!
    @IBOutlet weak var btn_pickup: UIButton!
    @IBOutlet weak var btn_wagon: UIButton!
    @IBOutlet weak var btn_coupe: UIButton!
    //--------------------Transmission type -------------------
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var view_Alert_brands: UIView!
    @IBOutlet weak var colView_brands: UICollectionView!
    @IBOutlet weak var btn_done_brands: UIButton!
    @IBOutlet weak var view_paging_unfocused: UIView!
    @IBOutlet weak var view_paging_focused: UIView!

    @IBOutlet weak var slider_price_range: UISlider!{
        didSet{
        slider_price_range.setThumbImage(image_dot_pink, for: .normal)
        slider_price_range.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    


    
    var arr_car_brands : [String] = []
    var arrcheckedM : [Bool] = []

    @IBOutlet weak var view_bg_car_brands: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        arr_car_brands = ["Hyundai","Tata","Audi","Bentaly","Kia","Lexus","Mazda","Nissan","Ford","Honda","Maruti","Isuzu","Land Rover","Tata","Maxus","Mercedes Benz","Mitsbishi","Perodua","Cheverlet","Suzuki","Toyota","Mahindra","Proton","Subaru"]
    }
    
    override func viewWillLayoutSubviews() {
        setUIConfiguration()
    }
    
    func setUIConfiguration(){
        self.view_Alert_brands.isHidden = true
        
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        btn_done_brands.layer.cornerRadius = self.btn_done_brands.frame.height / 2
        
        view_paging_focused.layer.cornerRadius = self.view_paging_focused.frame.height / 2
        view_paging_unfocused.layer.cornerRadius = self.view_paging_unfocused.frame.height / 2


        btn_MPV.layer.cornerRadius = 12
        btn_SUV.layer.cornerRadius = 12
        btn_auto.layer.cornerRadius = 12
        btn_coupe.layer.cornerRadius = 12
        btn_sedan.layer.cornerRadius = 12
        btn_wagon.layer.cornerRadius = 12
        btn_manual.layer.cornerRadius = 12
        btn_pickup.layer.cornerRadius = 12

        btn_MPV.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_SUV.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_coupe.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_sedan.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_wagon.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_manual.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_pickup.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_submit.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        btn_auto.addShadowWithExtraWidth(color: COLOR_SYSTEM_GRAY_2)
        
        view_bg_car_brands.layer.cornerRadius = 24
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    
    
    override func viewDidAppear(_ animated: Bool) {
         
        self.arrcheckedM = []
        for i in 0...self.arr_car_brands.count - 1{
            self.arrcheckedM.append(false)
        }
        
        print("Array checked",self.arrcheckedM)
    }
    
    @IBAction func submit_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "Home_VC") as! Home_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func done_Action_brands(_ sender: Any) {
        self.view_Alert_brands.isHidden = true

    }
    
    
    @IBAction func close_alert_brands(_ sender: Any) {
        self.view_Alert_brands.isHidden = true

    }
    
    @IBAction func select_car_brands(_ sender: Any) {
        self.view_Alert_brands.isHidden = false
    }
    
}


extension YourComfort_VC : UICollectionViewDelegate,UICollectionViewDataSource{
    // MARK: - UICollectionViewDataSource protocol
    
    // tell the collection view how many cells to make
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       return arr_car_brands.count
    }
    
    
    
    // make a cell for each cell index path
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Car_Brands_Cell", for: indexPath as IndexPath) as! Car_Brands_Cell
        
        cell.delegate = self
        cell.lbl_brand_value.text = self.arr_car_brands[indexPath.item]
        
        if arrcheckedM.count > 0{
            
            if self.arrcheckedM[indexPath.item]{
                print("true",arrcheckedM[indexPath.item])
                let image: UIImage = UIImage(named: "tick_pink")!
                cell.view_btn_bg.backgroundColor = .white
                cell.btn_checked.layer.borderColor = UIColor.clear.cgColor
                cell.btn_checked .setImage(image, for: UIControl.State.normal)
            }
            else{
                print("false",arrcheckedM[indexPath.item])
                let image: UIImage = UIImage(named: "tick_circle")!
                cell.view_btn_bg.backgroundColor = .lightGray
                cell.btn_checked.layer.borderColor = UIColor.lightGray.cgColor
                cell.btn_checked.layer.borderWidth = 2
                cell.btn_checked .setImage(image, for: UIControl.State.normal)
                
            }
        }
        
        return cell
    }
    
    
//    internal func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
//        return CGSize(width: (collectionView.frame.width / 2) - 10, height: 50) //add your height here
//    }
    
    
    func numberOfItemsInSection(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}


extension YourComfort_VC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.colView_brands.frame.width/2) - 10, height: 54)
    }
}


extension YourComfort_VC: CheckedCellDelegate {
    
    func checked(cell: Car_Brands_Cell) {
        let indexPath = self.colView_brands.indexPath(for: cell)
        print("Index",indexPath?.item)
        let cell = colView_brands!.cellForItem(at: indexPath!) as! Car_Brands_Cell
        let isChecked = arrcheckedM[indexPath!.item]
        
        if isChecked{
            let image: UIImage = UIImage(named: "tick_circle")!
            cell.view_btn_bg.backgroundColor = .lightGray
            cell.btn_checked.layer.borderColor = UIColor.lightGray.cgColor
            cell.btn_checked.layer.borderWidth = 2
            cell.btn_checked .setImage(image, for: UIControl.State.normal)
        }
        else{
            let image: UIImage = UIImage(named: "tick_pink")!
            cell.view_btn_bg.backgroundColor = .white
            cell.btn_checked.layer.borderColor = UIColor.clear.cgColor

            cell.btn_checked .setImage(image, for: UIControl.State.normal)
        }
        self.arrcheckedM[indexPath!.item] = !isChecked
        print(arrcheckedM)
    }
}
