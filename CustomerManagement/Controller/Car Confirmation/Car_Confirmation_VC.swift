//
//  Car_Confirmation_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 03/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class Car_Confirmation_VC: UIViewController {
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var view_Alert: UIView!
    @IBOutlet weak var btn_startDriving: UIButton!
    
    @IBOutlet weak var view_bottom: UIView!
    @IBOutlet weak var view_upper: UIView!
    @IBOutlet weak var btn_approve: UIButton!
    @IBOutlet weak var img_carPreview: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {

        setUIConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         navigationController?.setNavigationBarHidden(true, animated: animated)

     }
    
    
    func setUIConfiguration(){
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        
        self.btn_approve.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_approve.layer.cornerRadius = self.btn_approve.frame.height / 2
        btn_startDriving.layer.cornerRadius = self.btn_startDriving.frame.height / 2

        view_upper.layer.cornerRadius = 20
        view_bg.layer.cornerRadius = 20

        
        self.view_bottom.roundCorners_AtTopRight(radius: 34)
        self.view_Alert.isHidden = true

    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view_Alert.isHidden = true
    }

 
    @IBAction func clicked_StartDriving(_ sender: Any) {
        self.view_Alert.isHidden = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "RL_History_VC")as! RL_History_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func clicked_Approved(_ sender: Any) {
        self.view_Alert.isHidden = false
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
