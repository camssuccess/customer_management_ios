//
//  DriverTracking_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 31/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import MapKit
import Cosmos
import IHKeyboardAvoiding

struct Stadium {
    var name: String
    var lattitude: CLLocationDegrees
    var longtitude: CLLocationDegrees
}

class DriverTracking_VC: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var view_review_txtBG: UIView!
    @IBOutlet weak var btn_profile: UIButton!
    @IBOutlet weak var lbl_name_rating: UILabel!
    
    @IBOutlet weak var txtFeild_review: UITextField!
    @IBOutlet weak var view_give_rating: CosmosView!
    @IBOutlet weak var lbl_priceValue: UILabel!
    @IBOutlet weak var lbl_timeValue: UILabel!
    @IBOutlet weak var lbl_distanceValue: UILabel!
    @IBOutlet weak var btn_ptofile_rating: UIButton!
    @IBOutlet weak var view_rating_review: UIView!
    @IBOutlet weak var lbl_status: UILabel!
    @IBOutlet weak var lbl_estimated_time: UILabel!
    @IBOutlet weak var lbl_name: UILabel!
    @IBOutlet weak var view_rating: CosmosView!
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillLayoutSubviews() {
        self.setUIConfiguration()
        setCordination()
        self.view_rating_review.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view
        
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtFeild_review!.resignFirstResponder()
    }
    
    
    
    func setUIConfiguration(){
        self.btn_profile.contentMode = .scaleAspectFit
        //        self.btn_profile.addShadow(color: .gray)
        btn_profile.layer.cornerRadius = self.btn_profile.frame.height / 2
        btn_profile.layer.borderColor = UIColor.white.cgColor
        btn_profile.layer.borderWidth = 5
        
        view_rating.rating = 2
        
        //---------------- Set Rating view -------------
        view_review_txtBG.addShadowWithExtraWidth(color: .lightGray)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        self.txtFeild_review.delegate = self
        
        btn_ptofile_rating.layer.cornerRadius = self.btn_ptofile_rating.frame.height / 2
        btn_ptofile_rating.layer.borderColor = UIColor.white.cgColor
        btn_ptofile_rating.layer.borderWidth = 5
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2

        
        view_review_txtBG.layer.cornerRadius = self.view_review_txtBG.frame.height / 2

        //----------------------------------------------
    }
    
    func setCordination(){
        let coordinateOne = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 40.586746)!, longitude: CLLocationDegrees(exactly: -108.610891)!)
        let coordinateTwo = CLLocationCoordinate2D(latitude: CLLocationDegrees(exactly: 42.564874)!, longitude: CLLocationDegrees(exactly: -102.125547)!)
        //        self.getDirections(loc1: coordinateOne, loc2: coordinateTwo)
        // 1.
        mapView.delegate = self
        
        // 2.
        let sourceLocation = CLLocationCoordinate2D(latitude: 40.759011, longitude: -73.984472)
        let destinationLocation = CLLocationCoordinate2D(latitude: 40.748441, longitude: -73.985564)
        
        // 3.
        let sourcePlacemark = MKPlacemark(coordinate: sourceLocation, addressDictionary: nil)
        let destinationPlacemark = MKPlacemark(coordinate: destinationLocation, addressDictionary: nil)
        
        // 4.
        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
        
        // 5.
        let sourceAnnotation = MKPointAnnotation()
        sourceAnnotation.title = "Times Square"
        
        if let location = sourcePlacemark.location {
            sourceAnnotation.coordinate = location.coordinate
        }
        
        
        let destinationAnnotation = MKPointAnnotation()
        destinationAnnotation.title = "Empire State Building"
        
        if let location = destinationPlacemark.location {
            destinationAnnotation.coordinate = location.coordinate
        }
        
        // 6.
        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
        
        // 7.
        let directionRequest = MKDirections.Request()
        directionRequest.source = sourceMapItem
        directionRequest.destination = destinationMapItem
        directionRequest.transportType = .automobile
        
        // Calculate the direction
        let directions = MKDirections(request: directionRequest)
        
        // 8.
        directions.calculate {
            (response, error) -> Void in
            
            guard let response = response else {
                if let error = error {
                    print("Error: \(error)")
                }
                
                return
            }
            
            let route = response.routes[0]
            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
            
            let rect = route.polyline.boundingMapRect
            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
        }
        
    }
    
    
    
    
    
    func getDirections(loc1: CLLocationCoordinate2D, loc2: CLLocationCoordinate2D) {
        let source = MKMapItem(placemark: MKPlacemark(coordinate: loc1))
        source.name = "Your Location"
        let destination = MKMapItem(placemark: MKPlacemark(coordinate: loc2))
        destination.name = "Destination"
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay)
        
        renderer.strokeColor = UIColor(red: 17.0/255.0, green: 147.0/255.0, blue: 255.0/255.0, alpha: 1)
        
        renderer.lineWidth = 5.0
        
        return renderer
    }
    
    
    //    func showRouteOnMap(pickupCoordinate: CLLocationCoordinate2D, destinationCoordinate: CLLocationCoordinate2D) {
    //
    //        let sourcePlacemark = MKPlacemark(coordinate: pickupCoordinate, addressDictionary: nil)
    //        let destinationPlacemark = MKPlacemark(coordinate: destinationCoordinate, addressDictionary: nil)
    //
    //        let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
    //        let destinationMapItem = MKMapItem(placemark: destinationPlacemark)
    //
    //        let sourceAnnotation = MKPointAnnotation()
    //
    //        if let location = sourcePlacemark.location {
    //            sourceAnnotation.coordinate = location.coordinate
    //        }
    //
    //        let destinationAnnotation = MKPointAnnotation()
    //
    //        if let location = destinationPlacemark.location {
    //            destinationAnnotation.coordinate = location.coordinate
    //        }
    //
    //        self.mapView.showAnnotations([sourceAnnotation,destinationAnnotation], animated: true )
    //
    //        let directionRequest = MKDirections.Request()
    //        directionRequest.source = sourceMapItem
    //        directionRequest.destination = destinationMapItem
    //        directionRequest.transportType = .automobile
    //
    //        // Calculate the direction
    //        let directions = MKDirections(request: directionRequest)
    //
    //        directions.calculate {
    //            (response, error) -> Void in
    //
    //            guard let response = response else {
    //                if let error = error {
    //                    print("Error: \(error)")
    //                }
    //
    //                return
    //            }
    //
    //            let route = response.routes[0]
    //
    //            self.mapView.addOverlay((route.polyline), level: MKOverlayLevel.aboveRoads)
    //
    //            let rect = route.polyline.boundingMapRect
    //            self.mapView.setRegion(MKCoordinateRegion(rect), animated: true)
    //        }
    //    }
    
    @IBAction func submit_Action(_ sender: Any) {
//        self.view_rating_review.isHidden = true
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "BookingConfirmed_VC")
        vc?.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc!, animated: true)
        
    }
    
    @IBAction func profile_clicked(_ sender: Any) {
        self.view_rating_review.isHidden = false
    }
    
}


extension DriverTracking_VC : MKMapViewDelegate{
    
}
