//
//  SelectCar_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 28/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class SelectCar_VC: UIViewController {

    @IBOutlet weak var btn_selectNow: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        setUIConfiguration()
    }

    func setUIConfiguration(){
        self.btn_selectNow.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_selectNow.layer.cornerRadius = self.btn_selectNow.frame.height / 2
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

}
