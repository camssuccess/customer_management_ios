//
//  Booking_VC.swift
//  CustomerManagement
//
//  Created by KUMAR GAURAV on 07/09/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import Koyomi

class Booking_VC: UIViewController {
    fileprivate let invalidPeriodLength = 90
    @IBOutlet weak var view_alert: UIView!{
        didSet{
            view_alert.layer.cornerRadius = 6
        }
    }
    
    @IBOutlet weak var view_alert_bg: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var slider_hours: UISlider!{
        didSet{
            slider_hours.setThumbImage(image_dot_pink, for: .normal)
            slider_hours.setThumbImage(image_dot_pink, for: .highlighted) // Also change the image when dragging the slider
        }
    }
    @IBOutlet weak var btn_seeDrivers: UIButton!
    @IBOutlet weak var lbl_current_date: UILabel!
    @IBOutlet weak var calendar: Koyomi!{
        didSet{
//            calendar.circularViewDiameter = 0.2
            calendar.calendarDelegate = self
//            calendar.inset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            calendar.weeks = ("S", "M", "T", "W", "T", "F", "S")
            calendar.sectionSpace = 0
            calendar.style = .standard
            calendar.dayPosition = .center
//            calendar.selectionMode = .sequence(style: .semicircleEdge)
//            calendar.selectedStyleColor = UIColor(red: 203/255, green: 119/255, blue: 223/255, alpha: 1)
//            calendar
//                .setDayFont(size: 14)
//                .setWeekFont(size: 10)
        }
    }
    
    // MARK: - SelectionMode -

    public enum SelectionMode {
        case single(style: Style), multiple(style: Style), sequence(style: SequenceStyle), none
        
        public enum SequenceStyle { case background, circle, line, semicircleEdge }
        public enum Style { case background, circle, line }
    }

    // MARK: - ContentPosition -

    public enum ContentPosition {
        case topLeft, topCenter, topRight
        case left, center, right
        case bottomLeft, bottomCenter, bottomRight
        case custom(x: CGFloat, y: CGFloat)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print("height",self.view.frame.height + 200)
        scrollView.frame = self.view.frame
        scrollView.contentSize = CGSize(width: screenWidth, height: self.view.frame.height + 200)
    }
    
    override func viewWillLayoutSubviews() {
        setUIConfiguration()
        lbl_current_date.text = calendar.currentDateString()
        self.view_alert_bg.isHidden = true
    }
    
    func setUIConfiguration(){
        self.btn_seeDrivers.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_seeDrivers.layer.cornerRadius = self.btn_seeDrivers.frame.height / 2
        
        calendar.selectionMode = .multiple(style: .circle)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        self.view_alert_bg.isHidden = true
    }
    

    @IBAction func previous_month(_ sender: Any) {
    }
    
    @IBAction func next_month(_ sender: Any) {
    }
    
    @IBAction func seeDrivers_Action(_ sender: Any) {
        self.view_alert_bg.isHidden = false

    }
}


// MARK: - KoyomiDelegate -

extension Booking_VC: KoyomiDelegate {
    func koyomi(_ koyomi: Koyomi, didSelect date: Date?, forItemAt indexPath: IndexPath) {
        print("You Selected: \(date)")
    }
    
    func koyomi(_ koyomi: Koyomi, currentDateString dateString: String) {
        lbl_current_date.text = dateString
    }
    
    @objc(koyomi:shouldSelectDates:to:withPeriodLength:)
    func koyomi(_ koyomi: Koyomi, shouldSelectDates date: Date?, to toDate: Date?, withPeriodLength length: Int) -> Bool {
//        if calendar.currentDateString() <= date {
//            print("Your select day is invalid.")
//            return false
//        }
////
        if length > 90 {
            print("More than 90 days are invalid period.")
            return false
        }
        
        return true
    }
}
