//
//  VarifyIdenty_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding

class VarifyIdenty_VC: UIViewController {

    @IBOutlet weak var btn_verify: UIButton!
    @IBOutlet weak var btn_malayian: UIButton!
    @IBOutlet weak var btn_foreigner: UIButton!
    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var view_alert: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        KeyboardAvoiding.avoidingView = self.view_bg

    }
    override func viewWillLayoutSubviews() {
        self.setUIConfiguration()
        self.view_alert.isHidden = true

    }
    
    func setUIConfiguration(){
        self.btn_verify.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        self.btn_malayian.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        self.btn_foreigner.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        
        btn_verify.layer.cornerRadius = self.btn_verify.frame.height / 2
        btn_malayian.layer.cornerRadius = self.btn_malayian.frame.height / 2
        btn_foreigner.layer.cornerRadius = self.btn_foreigner.frame.height / 2
        
        view_bg.layer.cornerRadius = 16

        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.view_alert.isHidden = true
    }
    
    @IBAction func verify_Clicked(_ sender: Any) {
        self.view_alert.isHidden = false

    }
    

    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
