//
//  Login_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 25/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import FBSDKLoginKit
import GoogleSignIn
import IHKeyboardAvoiding

class Login_VC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var view_bg: UIView!
    @IBOutlet weak var btn_login: UIButton!
    @IBOutlet weak var btn_google: UIButton!
    @IBOutlet weak var btn_twitter: UIButton!
    @IBOutlet weak var btn_facebook: UIButton!
    @IBOutlet weak var txtField_password: UITextField!
    @IBOutlet weak var txtField_username: UITextField!
    var login_type = String()
    var email = String()
    var mobile = String()
    var user_name = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUIConfiguration()
        
        if let token = AccessToken.current,
            !token.isExpired {
            // User is logged in, do work such as go to next view controller.
        }
    }
    

    func setUIConfiguration(){
        self.btn_google.contentMode = .scaleAspectFit
        self.btn_login.addShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_login.layer.cornerRadius = self.btn_login.frame.height / 2
        btn_google.layer.cornerRadius = self.btn_google.frame.height / 2
        btn_twitter.layer.cornerRadius = self.btn_twitter.frame.height / 2
        btn_facebook.layer.cornerRadius = self.btn_facebook.frame.height / 2
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
        self.txtField_password.delegate = self
        self.txtField_username.delegate = self

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_username!.resignFirstResponder()
        txtField_password!.resignFirstResponder()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg

    }
    
      func facebooklogin() {
        let fbLoginManager : LoginManager = LoginManager()
         fbLoginManager.logIn(permissions: ["public_profile", "email"],from:self, handler: { (result, error) -> Void in

            print("\n\n result: \(String(describing: result))")
            print("\n\n Error: \(String(describing: error))")

         if (error == nil) {
            let fbloginresult : LoginManagerLoginResult = result!
           if(fbloginresult.isCancelled) {
              //Show Cancel alert
           } else if(fbloginresult.grantedPermissions.contains("email")) {
               self.returnUserData()
               //fbLoginManager.logOut()
           }
          }
        })
       }

    func returnUserData() {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"email,name"])
         graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            if ((error) != nil) {
                // Process error
                print("\n\n Error: \(String(describing: error))")
             } else {
                   let resultDic = result as! NSDictionary
                print("\n\n  fetched user: \(String(describing: result))")
                   if (resultDic.value(forKey:"name") != nil) {
                       let userName = resultDic.value(forKey:"name")! as! String as NSString?
                    print("\n User Name is: \(String(describing: userName))")
                    }

                   if (resultDic.value(forKey:"email") != nil) {
                       let userEmail = resultDic.value(forKey:"email")! as! String as NSString?
                    print("\n User Email is: \(String(describing: userEmail))")
                    }
               }
          })
       }
    
    
    @IBAction func facebook_Action(_ sender: Any) {
        self.facebooklogin()

    }
    @IBAction func google_Action(_ sender: Any) {
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
        
        // Automatically sign in the user.
        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
        GIDSignIn.sharedInstance()?.signIn()
        
    }
    
    @IBAction func twitter_Action(_ sender: Any) {
    }
    @IBAction func login_Action(_ sender: Any) {
    }
    @IBAction func register_Action(_ sender: Any) {
        let signUpVC = self.storyboard?.instantiateViewController(withIdentifier: "SignUp_VC")as! SignUp_VC
        signUpVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(signUpVC, animated: true)
    }
}


extension Login_VC : GIDSignInDelegate{
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
              withError error: Error!) {
      if let error = error {
        if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
          print("The user has not signed in before or they have since signed out.")
        } else {
          print("\(error.localizedDescription)")
        }
        return
      }
      // Perform any operations on signed in user here.
      let userId = user.userID                  // For client-side use only!
      let idToken = user.authentication.idToken // Safe to send to the server
        self.user_name = user.profile.name
      let givenName = user.profile.givenName
      let familyName = user.profile.familyName
        self.email = user.profile.email
        print("email",email)
        login_type = LOGIN_GOOGLE
//        self.validateMobileNumber_API()
      // ...
    }
    
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
              withError error: Error!) {
      // Perform any operations when the user disconnects from app here.
      // ...
    }

}
