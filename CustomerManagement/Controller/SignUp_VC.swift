//
//  SignUp_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 25/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import IHKeyboardAvoiding
class SignUp_VC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var txtField_password: UITextField!
    @IBOutlet weak var txtField_username: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    @IBOutlet weak var btn_google: UIButton!
    @IBOutlet weak var btn_twitter: UIButton!
    @IBOutlet weak var btn_facebook: UIButton!
    @IBOutlet weak var view_bg: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.setUIConfiguration()
    }
    

    func setUIConfiguration(){
        self.btn_google.contentMode = .scaleAspectFit
        self.btn_submit.addShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_submit.layer.cornerRadius = self.btn_submit.frame.height / 2
        btn_google.layer.cornerRadius = self.btn_google.frame.height / 2
        btn_twitter.layer.cornerRadius = self.btn_twitter.frame.height / 2
        btn_facebook.layer.cornerRadius = self.btn_facebook.frame.height / 2
        self.txtField_password.delegate = self
        self.txtField_username.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        KeyboardAvoiding.avoidingView = self.view_bg

    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        // handling code
        txtField_username!.resignFirstResponder()
        txtField_password!.resignFirstResponder()
    }

    @IBAction func facebook_Action(_ sender: Any) {
    }
    
    @IBAction func google_Action(_ sender: Any) {
    }
    
    @IBAction func twitter_Action(_ sender: Any) {
    }
    

  

    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func submit_Action(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "About_VC") as! About_VC
         vc.modalPresentationStyle  = .fullScreen
         self.navigationController?.pushViewController(vc, animated: true)
    }
}
