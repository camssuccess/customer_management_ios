//
//  Home_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 31/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class Home_VC: UIViewController {

    @IBOutlet weak var btn_appointment: UIButton!
    @IBOutlet weak var btn_wallets: UIButton!
    @IBOutlet weak var badge_rideLater: UIButton!
    @IBOutlet weak var badge_rideNow: UIButton!
    @IBOutlet weak var view_rideLater: UIView!
    @IBOutlet weak var view_ride_now: UIView!
    @IBOutlet weak var view_bg: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    override func viewWillLayoutSubviews() {
        setUIConfiguration()
    }
    
    func setUIConfiguration(){
        self.view_bg.roundCorners_AtTopRight(radius: 34)
        self.view_ride_now.layer.cornerRadius = 10
        self.view_rideLater.layer.cornerRadius = 10
        
        self.btn_wallets.layer.cornerRadius = 10
        self.btn_appointment.layer.cornerRadius = 10
        self.badge_rideNow.layer.cornerRadius = self.badge_rideNow.frame.height / 2
        self.badge_rideLater.layer.cornerRadius = self.badge_rideLater.frame.height / 2


//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
//        self.view.addGestureRecognizer(tap)
    }

    @IBAction func appointment_Action(_ sender: Any) {
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverTracking_VC") as! DriverTracking_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func wallet_Action(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DriverTracking_VC") as! DriverTracking_VC
        vc.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
