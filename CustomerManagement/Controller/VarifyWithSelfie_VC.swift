//
//  VarifyWithSelfie_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 26/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class VarifyWithSelfie_VC: UIViewController,AVCaptureMetadataOutputObjectsDelegate,AVCapturePhotoCaptureDelegate {
    
    @IBOutlet weak var camera_view: UIView!
    @IBOutlet weak var btn_veryfy: UIButton!
    
    //    var wrapper = OpenCVWrapper()
    var captureSession: AVCaptureSession!
    var stillImageOutput: AVCapturePhotoOutput!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var flipCamera : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        //        setupView()
        setUIConfiguration()
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //        checkPermissions()
        // Setup your camera here...
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium
        
//        let camera = getDevice(position: .front)
        setDevice(position: .front)

    }
    
    //Get the device (Front or Back)
    func setDevice(position: AVCaptureDevice.Position){
        do {
            let camera = getDevice(position: position)

            if camera != nil{
                let input = try AVCaptureDeviceInput(device: camera!)
                //Step 9
                stillImageOutput = AVCapturePhotoOutput()
                
                
                if captureSession.canAddInput(input) && captureSession.canAddOutput(stillImageOutput) {
                    captureSession.addInput(input)
                    captureSession.addOutput(stillImageOutput)
                    setupLivePreview()
                }
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera:  \(error.localizedDescription)")
        }
    }
    
    
    //Get the device (Front or Back)
    func getDevice(position: AVCaptureDevice.Position) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devices() as NSArray;
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            if(deviceConverted.position == position){
               return deviceConverted
            }
        }
       return nil
    }

    func setupLivePreview() {
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        
        videoPreviewLayer.videoGravity = .resizeAspectFill
        videoPreviewLayer.connection?.videoOrientation = .portrait
        camera_view.layer.addSublayer(videoPreviewLayer)
        
        
        let metadataOutput = AVCaptureMetadataOutput()
        if self.captureSession.canAddOutput(metadataOutput) {
            self.captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr, AVMetadataObject.ObjectType.ean13]
        } else {
            print("Could not add metadata output")
        }
        
        //Step12
        
        DispatchQueue.global(qos: .userInitiated).async { //[weak self] in
             self.captureSession.startRunning()
             //Step 13
            DispatchQueue.main.async {
                self.videoPreviewLayer.frame = self.camera_view.bounds
            }
         }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.captureSession.stopRunning()
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        
        guard let imageData = photo.fileDataRepresentation()
            else { return }
        
        let image = UIImage(data: imageData)

    }



    override func viewWillLayoutSubviews() {
        setUIConfiguration()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear

    }

    func setUIConfiguration(){
        self.btn_veryfy.addSideShadow(color: .gray)
        btn_veryfy.layer.cornerRadius = self.btn_veryfy.frame.height / 2
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func click_photo(_ sender: Any) {
       
//        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
//        stillImageOutput.capturePhoto(with: settings, delegate: self)
        
        
    }
    @IBAction func rotate_camera_Action(_ sender: Any) {
        
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = .medium

        flipCamera = !flipCamera
        if flipCamera{
            setDevice(position: .back)
        }
        else{
            setDevice(position: .front)
        }
        
    }
    
}




extension VarifyWithSelfie_VC: UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.originalImage] as? UIImage else { return }
//        let scannerViewController = ImageScannerController(image: image, delegate: self)
//        present(scannerViewController, animated: true)
    }
    

    
}
    
