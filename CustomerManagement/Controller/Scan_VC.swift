//
//  Scan_VC.swift
//  DriverManagement
//
//  Created by KUMAR GAURAV on 31/08/20.
//  Copyright © 2020 KUMAR GAURAV. All rights reserved.
//

import UIKit

class Scan_VC: UIViewController {

    @IBOutlet weak var btn_verify: UIButton!
    @IBOutlet weak var view_alert: UIView!
    @IBOutlet weak var view_alertBG: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillLayoutSubviews() {
        setUIConfiguration()
        view_alertBG.isHidden = true
    }
    
    func setUIConfiguration(){
        self.btn_verify.addSideShadow(color: COLOR_SYSTEM_GRAY_2)
        btn_verify.layer.cornerRadius = self.btn_verify.frame.height / 2
        view_alert.layer.cornerRadius = 10
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer? = nil) {
        self.view_alertBG.isHidden = true
    }
    
    @IBAction func verify_Action(_ sender: Any) {
        view_alertBG.isHidden = false
        
    }
    
    @IBAction func back_Action(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
